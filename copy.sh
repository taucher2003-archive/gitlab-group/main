#!/usr/bin/env bash

git clone ${CI_SERVER_PROTOCOL}://private-token:${GL_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${GL_SOURCE_NAMESPACE}/${GL_SOURCE}.git
git clone ${CI_SERVER_PROTOCOL}://private-token:${GL_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${GL_TARGET_NAMESPACE}/${GL_TARGET}.git

if [ ! -d "${GL_TARGET}/${GL_TARGET_DOC_FOLDER}" ]; then
  mkdir ${GL_TARGET}/${GL_TARGET_DOC_FOLDER}
fi
if [ ! -d "${GL_TARGET}/${GL_TARGET_DOC_FOLDER}/.vale" ]; then
  mkdir ${GL_TARGET}/${GL_TARGET_DOC_FOLDER}/.vale
fi
if [ ! -d "${GL_TARGET}/${GL_TARGET_DOC_FOLDER}/.vale/gitlab" ]; then
  mkdir ${GL_TARGET}/${GL_TARGET_DOC_FOLDER}/.vale/gitlab
fi

IFS=$'\n'
for filename in `find ./${GL_SOURCE}/doc/.vale/gitlab -type f`; do
  rawFilename=`basename $filename`
  if [[ "$GL_EXCLUDE_FILES" != *"${rawFilename}"* ]]; then
    cp $filename ${GL_TARGET}/${GL_TARGET_DOC_FOLDER}/.vale/gitlab
    echo "Copied $filename"
  else
    echo "Skipped $filename"
  fi
done

cd $GL_TARGET
git config --local user.email "${GL_BOT_EMAIL}"
git config --local user.name "${GL_BOT_NAME}"
git add .
git commit -m "${GL_COMMIT_MESSAGE:-Update Linting Config with latest changes}"
git push origin HEAD:$GL_PUSH_BRANCH -f

curl -X POST -H "Private-Token: ${GL_TOKEN}" -H "Content-Type: application/json" -d "{\"source_branch\": \"${GL_PUSH_BRANCH}\", \"target_branch\": \"${GL_TARGET_DEFAULT_BRANCH}\", \"title\": \"Update Linting Config\", \"remove_source_branch\": true}" ${CI_API_V4_URL}/projects/${GL_TARGET_PROJECT_ID}/merge_requests